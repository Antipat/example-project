<?php

namespace controllers;

use models\renderer\RendererDefault;

class NotFound
{

    public function __construct()
    {
        require_once 'models\renderer\RendererDefault.php';
        $this->renderer = RendererDefault::getInstance();
    }

    public function render($params = array()): void
    {
        $this->renderer->setHeadTitle('Страница не найдена');
        $this->renderer->setBody('404');
        $this->renderer->renderTemplate();
    }

}
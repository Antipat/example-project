<?php


namespace controllers;

use models\renderer\RendererDefault;
use models\user\UserModel;

class User
{

    public function __construct($params = array())
    {
        require_once 'models\renderer\RendererDefault.php';
        require_once 'models\user\UserModel.php';
        $this->renderer = RendererDefault::getInstance();
        $this->model = new UserModel();

        $this->initHandler($params);
    }

    private function initHandler(array $params)
    {
        $this->model->initHandler($params);
    }

    public function render($params = array()): void
    {
        $this->renderer->setHeadTitle('Пользователи');
        $this->renderer->setBody('users');
        $this->renderer->renderTemplate();
    }

}
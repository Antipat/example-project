<?php

namespace models\router;

interface RouterInterface
{

    public function route(): void;

} //interface RouterInterface
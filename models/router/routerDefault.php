<?php

namespace models\router;

require_once 'models/traits/singletonTrait.php';
require_once 'models\renderer\RendererDefault.php';

use controllers;
use models\renderer\RendererDefault;
use models\traits\SingletonTrait;

class RouterDefault implements RouterInterface
{
    use SingletonTrait;

    protected $protocol;
    private $baseUrl;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    protected function init($params = ['defaultController' => 'user']): void
    {
        $this->protocol = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://';
        $this->initBaseUrl();
        $this->initBasePath();
        $this->defaultController = $params['defaultController'];
        $this->renderer = RendererDefault::getInstance();
        $this->renderer->setHeadBaseUrl($this->baseUrl);
    }

    public function route(): void
    {
        $segmentsUrl = $this->getSegmentsUrl();
        try {
            if (!empty($segmentsUrl[0]))
                $controller = $this->loadController($segmentsUrl);
            else
                $controller = $this->loadController([$this->defaultController]);
        } catch (\Exception $e) {
            $controller = $this->loadController(['notFound']);
        } finally {
            $this->checkAjax();
            $controller->render();
        }
    }

    private function initBaseUrl(): void
    {
        $dir = __DIR__;
        $baseUrl = "";
        $dir = str_replace('\\', '/', realpath($dir));

        //add HTTPS or HTTP
        $baseUrl .= $this->protocol;

        //add host
        $baseUrl .= $_SERVER['HTTP_HOST'];

        //add alias
        if (!empty($_SERVER['CONTEXT_PREFIX'])) {
            $baseUrl .= $_SERVER['CONTEXT_PREFIX'];
            $baseUrl .= substr($dir, strlen($_SERVER['CONTEXT_DOCUMENT_ROOT']));
        } else {
            $baseUrl .= substr($dir, strlen($_SERVER['DOCUMENT_ROOT']));
        }

        $baseUrl = substr($baseUrl, 0, -13);

        $this->baseUrl = $baseUrl;
    } //method initBaseUrl

    private function initBasePath(): void
    {
        $this->basePath = dirname(realpath($_SERVER['SCRIPT_FILENAME'])) . '\\';
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    private function getSegmentsUrl(): array
    {
        return explode('/', $this->getRouteUrl());
    }

    private function getRouteUrl(): string
    {
        $actualUrl = $this->protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        return str_replace($this->baseUrl, '', $actualUrl);
    }

    private function loadController(array $segmentsUrl)
    {
        $controller_name = array_shift($segmentsUrl);
        $controller_path = "controllers\\$controller_name";
        try {
            return $this->loadControllerFile($controller_path, $segmentsUrl);
        } catch (\Exeption $e) {
            throw $e;
        }
    }

    private function checkAjax()
    {
        if (isAjax())
            exit;
    }

    private function loadControllerFile($path, $params)
    {
        $fullPath = $path . '.php';
        if (file_exists($fullPath)) {
            require_once $fullPath;
            return new $path($params);
        } else
            throw new \Exception('Controller file not found');
    }

} //class RouterDefault
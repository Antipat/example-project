<?php

namespace models\user;

use models\db\DbPdo;

class UserOrm
{
    private $table = 'users';
    private $structure = array(
        'name' => '',
        'email' => '',
        'tel' => '',
    );

    //default grouping result data by id
    private $group_array = array(
        'value' => 'id',
        'array' => false,
        'second_key' => '',
        'only_key' => '',
    );


    public function __construct($params = array())
    {
        require_once 'models\db\dbPdo.php';
        $this->db = DbPdo::getInstance();
    }

    function get($params = array()): array
    {

        $sql = '';
        $sql_data = '';
        $join = '';
        $where = '';
        $alias = 'u';
        $result = array();

        //find users by id
        if (!empty($params['id'])) {

            if (empty($where))
                $where = 'WHERE ';
            else
                $where .= ' AND ';

            $where .= " $alias.id IN (" . $this->db->implode_sql_data($params['id']) . ")";

        }

        //find users by name
        if (!empty($params['name'])) {

            if (empty($where))
                $where = 'WHERE ';
            else
                $where .= ' AND ';

            $where .= " $alias.name IN (" . $this->db->implode_sql_data($params['name']) . ")";

        }

        //find users by email
        if (!empty($params['email'])) {

            if (empty($where))
                $where = 'WHERE ';
            else
                $where .= ' AND ';

            $where .= " $alias.email IN (" . $this->db->implode_sql_data($params['email']) . ")";

        }

        //find user by tel
        if (!empty($params['tel'])) {

            if (empty($where))
                $where = 'WHERE ';
            else
                $where .= ' AND ';

            $where .= " $alias.tel IN (" . $this->db->implode_sql_data($params['tel']) . ")";

        }

        $sql = "SELECT $alias.*
			$sql_data
			FROM `$this->table` $alias
			$join 
			$where";

        //make DB-query and get response
        $result = $this->db->query($sql);

        //groupping result
        $result = group_array($result, $this->group_array['value'], $this->group_array['array'], $this->group_array['second_key'], $this->group_array['only_key']);

        return $result;

    } //method get

    public function save(array $params): array
    {
        $result = array();

        if (!empty($params)) {

            $formatData = merge_default($this->structure, $params);
            if (!empty($params['id'])) {

                $item = $this->get(['id' => $params['id']]);

                if (!empty($item)) {
                    $params_for_update = array_keys($this->structure);
                    $where_for_update = array('id');

                    $data = $params;
                    $item_id = $params['id'];

                    unset($data['id']);
                    $data['id'] = $item_id;

                    //update user data
                    $this->db->update($this->table, $params_for_update, $where_for_update, $data);
                }

            } else {

                unset($params['id']);
                //create sql for insert new row user
                $sql = 'INSERT INTO `' . $this->table . '`
                    (' . implode(',', array_keys($formatData)) . ')
                    VALUES (' . $this->db->implode_sql_data($formatData) . ')';

                $this->db->query($sql);
                $item_id = $this->db->getInsertId();

            }

            if (!empty($item_id))
                $result = $this->get(['id' => $item_id]);

        }

        return $result;
    }

    public function delete(array $itemIds)
    {
        $result = false;

        if (!empty($itemIds)) {

            $items = $this->get(['id' => $itemIds]);
            if (!empty($items)) {
                $ids = array();

                foreach ($items as $item) {
                    array_push($ids, $item['id']);
                }

                $sql = 'DELETE FROM `' . $this->table . '` WHERE id IN(' . $this->db->implode_sql_data($ids) . ')';
                $this->db->query($sql);
                $result = array();
            }
        }

        return $result;
    }

}
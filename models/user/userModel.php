<?php


namespace models\user;

class UserModel
{

    public function __construct($params = array())
    {
        require_once 'models\user\UserOrm.php';
        $this->orm = new UserOrm();
    }

    public function initHandler(array $params): array
    {
        $result = array();

        if (!empty($params)) {
            $method = array_shift($params);
            switch ($method) {
                case 'save':
                    $result = $this->save($params);
                    break;
                case 'search':
                    $result = $this->search($params);
                    break;
                case 'delete':
                    $result = $this->delete($params);
                    break;
                default:
                    break;
            }
        }

        if (isAjax())
            echo json_encode($result);

        return $result;
    }

    private function save(array $params)
    {
        $post = $_POST;
        $result = array();
        $is_valid = $this->validationSave($post);

        if ($is_valid) {
            $result = $this->orm->save($post);
        }

        return $result;
    }

    private
    function search(array $params)
    {
        $post = $_POST;
        return $this->orm->get($post);
    }

    private
    function delete(array $params)
    {
        $post = $_POST;
        return $this->orm->delete($post);
    }

    private function validationSave(array $item): bool
    {
        if (!$this->validateName($item))
            return false;
        if (!$this->validateEmail($item))
            return false;
        if (!$this->validateTel($item))
            return false;

        return true;
    }

    private function validateName(array $item)
    {

        if (empty($item['name']))
            return false;

        if (gettype($item['name']) !== 'string')
            return false;

        if (strlen($item['name']) < 4)
            return false;

        return true;
    }

    private function validateEmail(array $item)
    {
        if (empty($item['email']))
            return false;

        if (gettype($item['email']) !== 'string')
            return false;

        if (strlen($item['email']) < 8)
            return false;

        if (!empty($item['id'])) {

            if (!empty($item['email'])) {

                //check unique email
                $temp = $this->search([
                    'email' => $item['email']
                ]);

                if (!empty($temp) && $item['id'] != current($temp)['id'])
                    return false;
            }

        } else {

            if (!empty($item['email'])) {

                //check unique email
                $temp = $this->search([
                    'email' => $item['email']
                ]);
                if (!empty($temp))
                    return false;

            }

        }

        return true;
    }

    private function validateTel(array $item)
    {

        if (empty($item['tel']))
            return false;

        if (gettype($item['tel']) !== 'string')
            return false;

        $pattern = '/\+?([0-9]{2})-?([0-9]{3})-?([0-9]{6,7})/';
        preg_match($pattern, $item['tel'], $matches, PREG_OFFSET_CAPTURE);

        if (empty($matches))
            return false;

        return true;
    }


}
<?php

function isAjax()
{
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        return true;

    return false;
}

function merge_default($default, $params, $copy_default = true)
{

    $options = array();
    $optionsNames = array_keys($default);
    foreach ($optionsNames as $o) {

        if (!isset($params[$o])) {

            if ($copy_default)
                $options[$o] = $default[$o];

        } else {

            $options[$o] = $params[$o];

        }
    }

    return $options;

} //func merge_default()

/*
*   group array by key
*   if var 'group' true - create multidimensional array
*/
function group_array($list, $key, $group = true, $second_key = 'id', $only_key = false)
{

    $result = array();

    foreach ($list as $index => $value) {

        if (!empty($value[$key]) || $value[$key] === 0 || $value[$key] === '0') {

            if ($group) {

                if (empty($result[$value[$key]])) $result[$value[$key]] = array();

                if (!empty($value[$second_key])) {

                    if ($only_key)
                        $result[$value[$key]][$value[$second_key]] = $value[$second_key];
                    else
                        $result[$value[$key]][$value[$second_key]] = $value;

                } else
                    array_push($result[$value[$key]], $value);

            } else {

                $result[$value[$key]] = $value;

            }

        }

        unset($list[$index]);

    }

    return $result;

} //function group_array()

<?php

namespace models\renderer;

interface RendererInterface
{

    //public function renderHead();

    //public function renderHeader();

    //public function renderBody();

    public function renderTemplate(): void;

    //public function renderFooter();

}
<?php


namespace models\renderer;

require_once 'models/traits/singletonTrait.php';

use models\traits\SingletonTrait;

class RendererDefault implements RendererInterface
{
    use SingletonTrait;

    protected $viewsPath = 'views/';
    protected $layoutPath;
    protected $pagesPath;
    protected $rendererPartsList = [
        'head' => '',
        'header' => '',
        'body' => '',
        'footer' => '',
    ];
    protected $rendererParamsList = [
        'head' => [],
        'header' => [],
        'body' => [],
        'footer' => [],
    ];
    private $temp;


    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    protected function init(): void
    {
        $this->layoutPath = $this->viewsPath . 'layouts/';
        $this->pagesPath = $this->viewsPath . 'pages/';
    }

    public function renderTemplate(): void
    {

        $head = $this->getHeadPart();
        $header = $this->getHeaderPart();
        $body = $this->getBodyPart();
        $footer = $this->getFooterPart();

        include $this->viewsPath . 'template.php';
    }

    protected function getHeadPart(): string
    {
        return $this->getPart('head');
    }

    protected function getHeaderPart(): string
    {
        return $this->getPart('header');
    }

    protected function getBodyPart(): string
    {
        return $this->getPart('body');
    }

    protected function getFooterPart(): string
    {
        return $this->getPart('footer');
    }

    public function setHead(string $content, $params = array(), $is_file = true): void
    {
        $this->setPart('head', $this->layoutPath, $content, $params, $is_file);
    }

    public function setHeader(string $content, $params = array(), $is_file = true): void
    {
        $this->setPart('header', $this->layoutPath, $content, $params, $is_file);
    }

    public function setBody(string $content, $params = array(), $is_file = true): void
    {
        $this->setPart('body', $this->pagesPath, $content, $params, $is_file);
    }

    public function setFooter(string $content, $params = array(), $is_file = true): void
    {
        $this->setPart('footer', $this->layoutPath, $content, $params, $is_file);
    }

    protected function setPart(string $partName, string $path, string $content, $params = array(), $is_file = true): void
    {
        if ($is_file) {
            extract($params, EXTR_SKIP);
            extract($this->rendererParamsList[$partName], EXTR_SKIP);
            ob_start();
            require($path . $content . '.php');
            $this->rendererPartsList[$partName] = ob_get_clean();
        } else {
            $this->rendererPartsList[$partName] = $content;
        }
    }

    protected function getPart(string $part): string
    {
        if (empty($this->rendererPartsList[$part])) {
            $functionName = 'set' . ucfirst($part);
            $this->$functionName($part);
        }

        return $this->rendererPartsList[$part];
    }

    public function setHeadTitle(string $title): void
    {
        $this->rendererParamsList['head']['title'] = $title;
    }

    public function setHeadBaseUrl(string $baseUrl): void
    {
        $this->rendererParamsList['head']['baseUrl'] = $baseUrl;
    }

}
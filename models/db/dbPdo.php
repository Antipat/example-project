<?php

namespace models\db;

require_once 'models/traits/singletonTrait.php';

use PDO;
use models\traits\SingletonTrait;

class DbPdo implements DbInterface
{
    use SingletonTrait;

    protected $connect;
    protected $host = 'localhost';
    protected $dbName = 'example';
    protected $user = 'root';
    protected $password = '';

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    protected function init(): void
    {

        //Database PDO connection
        $pdo_settings = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        );

        $dsn = 'mysql:host=' . $this->host .
            ';dbname=' . $this->dbName .
            ';charset=utf8';

        //create connect DB for PDO
        $this->connect = new PDO($dsn, $this->user, $this->password, $pdo_settings);
    }

    /**
     * Execute db query
     */
    public function query(string $params): array
    {

        $result = array();

        if (!empty($params)) {

            $response = $this->connect->query($params);

            if (!empty($response) && !empty($response->queryString)) {

                if (strpos($response->queryString, 'SELECT') !== false) {

                    while ($row = $response->fetch()) {

                        $result[] = $row;
                    }

                }

            }

        }

        return $result;

    } //method select

    /**
     * Execute update db query
     */
    public function update(string $table, array $params, array $where, array $data): bool
    {
        $result = false;

        if (
            !empty($table) &&
            !empty($params) &&
            !empty($where) &&
            !empty($data)
        ) {

            $split = '=?,';
            $sql = "UPDATE `" . $table . "` SET " .
                $this->implode_sql_data_ex(array('data' => $params, 'split' => $split, 'wrapp' => '`', 'delete_one_char' => true)) .
                " WHERE " .
                $this->implode_sql_data_ex(array('data' => $where, 'split' => $split, 'wrapp' => '`', 'delete_one_char' => true));

            if ($statement = $this->connect->prepare($sql)) {
                //reset keys when array assoc
                //need for PDO->execute()
                if (!isset($data[0]))
                    $data = array_values($data);

                $statement->execute($data);

                $result = true;
            }

        }

        return $result;
    } //method update

    public function implode_sql_data_ex($params = array(
        'data' => array(),
        'key' => null,
        'wrapp' => "'",
        'split' => ',',
        'delete_one_char' => false,
    ))
    {

        $result = '';

        $type = gettype($params['data']);

        if ($type == 'string' || $type == 'integer' || $type == 'double' || $type == 'float') {

            $result .= $params['wrapp'] . $params['data'] . $params['wrapp'];

        } else if ($type == 'array') {

            foreach ($params['data'] as $value) {

                if (!empty($key) && is_array($value) && !(is_array($value[$key])))
                    $result .= $params['wrapp'] . $value[$key] . $params['wrapp'] . $params['split'];

                else
                    $result .= $params['wrapp'] . $value . $params['wrapp'] . $params['split'];

            }

            $count_delete_char = strlen($params['split']);

            if ($params['delete_one_char'])
                $count_delete_char = 1;

            $result = substr($result, 0, -$count_delete_char);

        }

        return $result;

    } //method implode_sql_data_ex

    public function implode_sql_data($data, $key = null, $split = ',', $delete_one_char = false)
    {

        $result = '';

        $type = gettype($data);

        if ($type == 'string' || $type == 'integer' || $type == 'double' || $type == 'float') {

            $result .= "'" . $data . "'";

        } else if ($type == 'array') {

            foreach ($data as $value) {

                if (!empty($key) && is_array($value) && !(is_array($value[$key])))
                    $result .= "'" . $value[$key] . "'" . $split;

                else
                    $result .= "'" . $value . "'" . $split;

            }

            $count_delete_char = strlen($split);

            if ($delete_one_char)
                $count_delete_char = 1;

            $result = substr($result, 0, -$count_delete_char);

        }

        return $result;

    } //method implode_sql_data

    /**
     * Return last insert id
     */
    public function getInsertId(): int
    {
        return $this->connect->lastInsertId();
    }

} //class DbPdo
<?php

namespace models\db;

interface DbInterface
{

    /**
     * Execute db query
     */
    public function query(string $params): array;

    /**
     * Execute update db query
     */
    public function update(string $table, array $params, array $where, array $data): bool;

    /**
     * Return last insert id
     */
    public function getInsertId(): int;

} //interface DbInterface

?>

<?php

namespace models\traits;

trait SingletonTrait
{

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    protected static $_instance;
    abstract protected function init();

    /**
     * Get instance singleton
     */
    public static function getInstance(): self
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
            self::$_instance->init();
        }

        return self::$_instance;
    }
}

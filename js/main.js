inputs = {

    //value for the case checkbox or radio not selected
    no_value: '0',

    //find all inputs inside $sel and return values as object
    //result object:
    //key -> input name, value -> input value
    get: function ($sel) {

        var $obj = this;

        //list of all inputs
        var list = $($sel).find('input, textarea, select');

        //result object
        var result = {};

        //special temp array for radio inputs (to return if all radio unchecked)
        var radio_tmp = {};

        //loop inputs
        list.each(function (i) {

            var el = $(this);

            //check is current element radio
            if (el.attr('type') == 'radio') {
                if (el.prop('checked'))
                    result[el.attr('name')] = el.val();
                else
                    radio_tmp[el.attr('name')] = $obj.no_value;
            }
            //is current element checkbox
            else if (el.attr('type') == 'checkbox') {
                var val = $obj.no_value;
                if (el.prop('checked')) val = el.val();

                result[el.attr('name')] = val;
            }
            //is current element textarea
            else if (el.is('textarea')) {
                var val = el.val();
                result[el.attr('name')] = val;
            }
            //all other elements just get val()
            else {
                result[el.attr('name')] = el.val();
            }

        });

        //add no value for radio groups which are fully unchecked
        for (var i in radio_tmp) if (typeof result[i] == 'undefined') result[i] = radio_tmp[i];

        return result;

    }, //func get

    //find all inputs inside $sel and fill values
    //expect $data as object where:
    //key -> input name, value -> input value
    set: function ($sel, $data) {

        var $obj = this;

        //list of all inputs
        var list = $($sel).find('input, textarea, select, img');

        //loop inputs
        list.each(function (i) {

            var el = $(this);

            //check is there exist some value in $data for current input
            if (typeof ($data[el.attr('name')]) != 'undefined') {

                var cur_val = $data[el.attr('name')];

                //check is current element radio or checkbox
                if (el.attr('type') == 'radio' || el.attr('type') == 'checkbox') {

                    if (cur_val == el.val() || cur_val == 1 || cur_val == true || cur_val == 'true') el.prop('checked', 'checked');
                    else el.prop('checked', '');

                }

                //all other elements just get val()
                else {
                    el.val(cur_val);
                    el.attr('value', cur_val);
                }

            }
            //check is current element should be img ($data[el.attr('title')] value not undefined)
            else if (el.is('img') && typeof ($data[el.attr('title')]) != 'undefined') {
                el.attr('src', base_url + el.attr('rel') + $data[el.attr('title')]);
            }

        });

    }, //func set

}; //object inputs

post = function (url, data, success, error) {
    //__(url, data, success, error);

    success = success || function (message) {
    };
    error = error || function (message) {
    };

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: success,
        error: error,
        dataType: 'json'
    });

}; //func post


sortSelector = function ($selector, sort_by, callback) {

    callback = callback || function () {
    };

    if (!($selector instanceof jQuery))
        $selector = $($selector);

    var rows = $selector;

    var sort_order = 1;

    if (sort_by[0] === '-') {

        sort_order = -1;
        sort_by = sort_by.substr(1);

    }

    rows.sort(function (a, b) {

        var compA = jQuery(a).attr(sort_by);
        var compB = jQuery(b).attr(sort_by)

        //__(sort_by, compA, compB);

        //check on valid number value
        //sort like number
        if (typeof (compA * 1) == 'number' && !(isNaN(compA * 1)) && compA * 1 != Infinity) {

            compA = parseInt(compA, 10);
            compB = parseInt(compB, 10);

            var intRegex = /^-?\d+$/;

            /*
            if(sort_by == 'data-order'){
                __('numeric');
                __(compA);
                __(compB);
            }
            */

            if (!intRegex.test(compA)) {
                compA = 0;
            }
            if (!intRegex.test(compB)) {
                compB = 0;
            }

            if (sort_order == 1)
                return (compA < compB) ? 1 : (compA > compB) ? -1 : 0;
            else
                return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;

        }

        //sort like string
        else {

            //__(compA, compB, compA.localeCompare(compB));

            compA = compA.toLocaleLowerCase();
            compB = compB.toLocaleLowerCase();

            if (sort_order == 1)
                return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
            else
                return (compA < compB) ? 1 : (compA > compB) ? -1 : 0;

        }

    });

    jQuery.each(rows, function (i) {

        //__($(this));
        $(this).parent().append(this);
        callback(i, $(this));

    });

}; //func sortSelector
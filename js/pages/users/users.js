users = {

    usersGetUrl: 'user/search/',
    usersList: {},
    sortState: 0,
    userDefaultData: {
        id: '',
        name: '',
        email: '',
        tel: '',
    },

    init: function () {
        const self = this;

        self.$usersTableBody = jQuery('.usersTableBody');
        self.$usersSearchForm = jQuery('.usersSearchForm');
        self.$usersHeadTableName = jQuery('.userTableHeadName');
        self.$userModal = jQuery('#userModal');
        self.search();
        self.initSortByName();
    },

    initSortByName: function () {
        const self = this;
        self.$usersHeadTableName.click(function () {
            self.sortUsersByName();
        });
    },

    search: function (params) {
        const self = this;
        params = params || {};

        console.log('Request search()', params);
        post(
            self.usersGetUrl,
            params,
            function (response) {
                console.log('Response search()', response);
                self.usersList = response;
                self.render(response);
            },
            function () {

            });
    },

    searchByForm: function () {
        const self = this;
        let data = self.getSearchFormData();
        self.search(data);
    },

    render: function (list) {
        const self = this;
        let html = '';

        for (let key in list) {
            html += self.getUserHtml(list[key]);
        }
        self.$usersTableBody.html(html);
    },

    getUserHtml: function (userElement) {
        const self = this;
        let html =
            '<tr class="userElement" data-name="' + userElement.name + '">' +
            '<td>' + userElement.name + '</td>' +
            '<td>' + userElement.email + '</td>' +
            '<td>' + userElement.tel + '</td>' +
            '<td>' + self.getUserActionsHtml(userElement.id) + '</td>' +
            '</tr>';

        return html;
    },

    getUserActionsHtml: function (userId) {
        let html =
            '<button class="userElementBtn btn btn-warning" title="Редактировать пользователя" onclick="users.editUser(' + userId + ')"><span class="fa fa-pencil"></span></button>' +
            '<button class="userElementBtn btn btn-danger" title="Удалить пользователя" onclick="users.deleteUser(' + userId + ')"><span class="fa fa-trash"></span></button>';

        return html;
    },

    getSearchFormData: function () {
        const self = this;
        return inputs.get(self.$usersSearchForm);
    },

    editUser: function (userId) {
        const self = this;
        let data;

        if (self.usersList[userId]) {
            data = self.usersList[userId];
            self.$userModal.find('#userModalCreateLabel').hide();
            self.$userModal.find('#userModalEditLabel').show();
        } else {
            data = self.userDefaultData;
            self.$userModal.find('#userModalEditLabel').hide();
            self.$userModal.find('#userModalCreateLabel').show();
        }

        inputs.set(self.$userModal, data);
        self.$userModal.modal();
    },

    deleteUser: function (userId) {
        const self = this;

        post(
            'user/delete/',
            {userId},
            function (response) {
                console.log('Response deleteUser()', response);
                delete self.usersList[userId];
                self.render(self.usersList);
            },
            function (error) {
                console.error('Response deleteUser error');
            });
    },

    sortUsersByName: function () {
        const self = this;
        const selector = $('.userElement');
        let sortBy = '';
        const icon = self.$usersHeadTableName.find('span[name=icon]');

        if (self.sortState == 1) {
            self.sortState = 2;
            sortBy = '-data-name';
            icon.attr('class', 'fa fa-arrow-up');
        } else {
            self.sortState = 1;
            sortBy = 'data-name';
            icon.attr('class', 'fa fa-arrow-down');
        }

        sortSelector(selector, sortBy);
    },

};

jQuery(function () {

    users.init();

});
<?php
include 'views/widgets/users/actionPanel.php';
include 'views/widgets/users/userModal.php';
?>

<link href="css/pages/users/users.css" rel="stylesheet">
<script src="js/pages/users/users.js"></script>

<div id="usersContainer">

    <div class="usersBlock">

        <table class="usersTable table">
            <thead class="thead-dark usersTableHead">
            <tr>
                <th class="userTableHeadName">Имя <span name="icon" class="fa fa-sort"></span></th>
                <th>email</th>
                <th>Телефон</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody class="usersTableBody">

            </tbody>
        </table>

    </div>

</div>
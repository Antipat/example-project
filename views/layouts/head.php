<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-language" content="ru"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="description" content=""/>
<meta name="keywords" content=""/>
<meta name="theme-color" content="#0d9f40">

<?php if (!empty($metadataLoad)) echo $metadataLoad; ?>

<title><?php if (!empty($title)) echo $title; ?></title>

<?php if (!empty($baseUrl)) echo "<script> const baseUrl = '{$baseUrl}';</script>"; ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="js/main.js"></script>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/pages/widgets/users/userModal.css" rel="stylesheet">
<script src="js/pages/widgets/users/userModal.js"></script>

<div class="modal fade" id="userModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userModalCreateLabel">Создание пользователя</h5>
                <h5 class="modal-title" id="userModalEditLabel">Редактирование пользователя</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id">
                <div class="formControlModalWrapper">
                    <input type="text" name="name" class="form-control" placeholder="Имя" title="Имя пользователя"
                           autocomplete="off">
                </div>
                <div class="formControlModalWrapper">
                    <input type="email" name="email" class="form-control" placeholder="email" title="email пользователя"
                           autocomplete="off">
                </div>
                <div class="formControlModalWrapper">
                    <input type="tel" name="tel" class="form-control" placeholder="Телефон"
                           title="Телефон пользователя" autocomplete="off">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary btnSave">Сохранить</button>
            </div>
        </div>
    </div>
</div>
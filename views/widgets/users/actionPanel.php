<link href="css/pages/widgets/users/actionPanel.css" rel="stylesheet">
<script src="js/pages/widgets/users/actionPanel.js"></script>

<nav class="usersActionPanel navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-nav">

        <div class="usersSearchForm">

            <span class="nav-item">
                <div class="input-group" title="Поиск по имени">
                    <input class="form-control py-2 border-right-0 border" name="name" type="search"
                           placeholder="Имя пользователя">
                    <span class="input-group-append">
                        <div class="input-group-text bg-transparent"><i class="fa fa-user"></i></div>
                    </span>
                </div>
            </span>

            <span class="nav-item">
                <div class="input-group" title="Поиск по email">
                    <input class="form-control py-2 border-right-0 border" name="email" type="search"
                           placeholder="email">
                    <span class="input-group-append">
                        <div class="input-group-text bg-transparent"><i class="fa fa-envelope"></i></div>
                    </span>
                </div>
            </span>

            <span class="nav-item">
                <div class="input-group" title="Поиск по телефону">
                    <input class="form-control py-2 border-right-0 border" name="tel" type="search"
                           placeholder="Телефон">
                    <span class="input-group-append">
                        <div class="input-group-text bg-transparent"><i class="fa fa-phone"></i></div>
                    </span>
                </div>
            </span>

            <li class="nav-item">
                <button class="btn btn-success searchBtn" title="Поиск">
                    <i class="fa fa-search"></i>
                </button>
            </li>

        </div>

        <li class="nav-item">
            <button class="btn btn-success" title="Добавить пользователя" onclick="users.editUser()">
                <i class="fa fa-user-plus"></i>
            </button>
        </li>

    </div>
</nav>
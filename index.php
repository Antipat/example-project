<?php

require_once 'autoload.php';
require_once 'models\general\main.php';
require_once 'models\db\dbPdo.php';
require_once 'models\router\routerDefault.php';
require_once 'models\renderer\rendererDefault.php';

$db = models\db\DbPdo::getInstance();
$router = models\router\RouterDefault::getInstance();
$renderer = models\renderer\RendererDefault::getInstance();

$router->route();

?>
